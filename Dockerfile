FROM directus/directus

WORKDIR /directus/

COPY ./uploads /directus/uploads

COPY ./extensions /directus/extensions

EXPOSE 8055
