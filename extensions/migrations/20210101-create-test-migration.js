module.exports = {
	async up(knex) {
		await knex.schema.createTable('test', (table) => {
            table.increments();
            table.string('name');
            table.timestamps();
		});
	},

	async down(knex) {
		await knex.schema.dropTable('test');
	},
};